import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

// import Vue from 'vue';
// import wrap from '@vue/web-component-wrapper'
// import SquareLocations from './components/SquareLocations.vue'
// const CustomElement = wrap(Vue, SquareLocations)
// window.customElements.define('square-locations', CustomElement)
